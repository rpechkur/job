Все данные приложения находятся в файле js/index.js, после изменения этого файла перекомпилируйте его в index.min.js.
Находятся они в объекте Vue (свойство data).
Чтобы загрузить данные из бек-енда в приложение, необходимо передать глобальной переменной json-строку, в которой будут необходиміе поля (ключи и глубина),
Данные из бек-енда всегда должны лежать в корне "payForm", как и данные фронт-енд части, то есть:
json_encode(['payForm' => ['fields' => [...], 'config' => [...]]]);

То есть бек-енд может передавать любые данные, которые есть в фронт-енд части приложения (соблюдая уровень вложености), все, которые пришли с бек-енда,
перезапишут фронтендовые.

На пример, чтобы передать переводы для формы, формируем php-массив:


$arr = [
    'payForm' => [
        'vocabulary' => [
                'payNum' => 'счет',
                'sum' => 'Сумма',
                'pay' => 'Оплатить',
                'process' => 'Обработка',
                'complete' => 'Завершить',
                'repeat' => 'Повторить',
                'printReceipt' => 'Распечатать квитанцию',
                'attention' => [
                    'message' => '<b>Обратите внимание!</b> Мы рекомендуем использовать карты Master Card для оплаты!',
                ]
            ],
            'fields' => [
                'cardNumber' => [
                    'label' => 'Номер карты',
                    'errorMessagesList' => [
                    'required' => 'Введите номер карты',
                        'invalidCard' => 'Неверный номер карты',
                        'blockedCard' => 'Карта заблокирована, свяжитесь с Вашим банком'
                    ],
                ],
                'expiryDate' => [
                    'label' => 'Срок действия',
                    'errorMessagesList' => [
                        'required' => 'Введите срок действия карты',
                        'expiration' => 'Срок действия карты истек',
                        'invalidExpiryDate' => 'Неверная дата'
                    ]
                ],
                'cardCvv' => [
                        'label' => 'CVV/CVC код',
                        'helperDescription' => 'CVV/CVC2 - последние <br> три цифры на обороте карты',
                        'errorMessagesList' => [
                            'invalidCvv' => 'Неверный код'
                        ]
                ]
            ]
    ]
];

и выводим его в json-кодированном формате в js-переменную XDATA:
json_encode($arr);

Таким образом передачи данных устанавливаются все значения, что есть в приложении. На пример, чтобы активировать режим 
readonly, необходимо передать свойство readonly необходимым полям и установить значения:

$arr = [
    'payForm' => [
            'fields' => [
                'cardNumber' => [
                    'rawValue' => '4444 4444 4444 4444' // передаем заполненное значение
                    'readonly' => true
                ],
                'expiryDate' => [
                    'rawValue' => '07/24' // передаем заполненное значение
                    'readonly' => true
                ]
            ]
    ]
];

json_encode($arr);


Пример данных в json (находится в файле index.html):

<script v-if="!this.payForm.config.dataReady">var XDATA = '{"payForm":{"vocabulary":{"payNum":"invoice","sum":"Total","pay":"Pay","attention":{"message":"<b>Pay attention!<\\/b> We recommend using Mastercard to pay!"}},"fields":{"cardNumber":{"label":"Card Number","errorMessagesList":{"required":"Please enter a card number first","invalidCard":"Invalid card number","blockedCard":"The card is blocked, contact your bank"}},"expiryDate":{"label":"Expiration Date","errorMessagesList":{"required":"Enter the expiration date","expiration":"The card has expired","invalidExpiryDate":"Invalid date"}},"cardCvv":{"label":"CVV\\/CVC Number","helperDescription":"CVV \\/ CVC2 - the last <br> three number on the card back","errorMessagesList":{"invalidCvv":"Invalid code"}}}}}';</script>

