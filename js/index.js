new Vue({
    el: '#payForm',
    data: {
        payForm: {
            // это основное свойство, содержащее переводы текста
            vocabulary: {
                payNum: 'счет',
                sum: 'Сумма',
                pay: 'Оплатить',
                process: 'Обработка',
                complete: 'Завершить',
                repeat: 'Повторить',
                printReceipt: 'Распечатать квитанцию',
                sentToEmail: 'Отправить на e-mail',
                attention: {
                    message: '<b>Обратите внимание!</b> Мы рекомендуем использовать карты MasterCard для оплаты!',
                    active: true // показывать или нет
                }
            },
            // данные об оплате
            payment: {
                order: 12345567,            // номер
                total: '10 UAH',            // общая сумма + валюта
                status: false,              // пройдена ли валидация банком всех данных по форме
                statusProcess: false,       // валидация банком всех полей формы в процессе
                confirmation: false,        // подтверждение оплаты ( оплата успешно произведена )
                confirmationProcess: false, // подтверждение оплаты в процессе
                confirmationReady: false,   // когда пришел ответ от банка об подтверждении оплаты (не путать со статусом)
                message: '',                // сообщение статуса ( Ошибка:/Ваш счет пополнен на )
                description: '',            // описание статуса ( описание ошибки )
                bankFrame: ''               // фрейм банка
            },
            config: {
                emailLink: 'abc@example.com',
                show: true, // Показвать форму
                dataReady: false, // Приложение готово к показу
                matchMedia: { xs: true, sm: false, md: false, lg: false, xl: false },
                queryHeaders: { 'Accept-Language' : 'en-US' },
            },
            cardTypes: {
                "0": {char: '4', value: 'VISA'},
                "1": {char: '5', value: 'MasterCard'},
                "2": {char: '9', value: 'ПРОСТІР'},
            },
            // данные об полях формы
            fields: {
                cardNumber: {
                    label: 'Номер карты',               // Это понятно
                    mask: "XXXX XXXX XXXX XXXX",        // маска
                    rawValue: '',                       // Значение
                    active: false,                      // поле активно ( заполняется сейчас )
                    error: false,                       // статус валидации
                    errorMessage: '',                   // Текущее сообщение к показу
                    errorMessagesList: {                // Список ошибок
                        required: 'Введите номер карты',
                        invalidCard: 'Неверный номер карты',
                        blockedCard: 'Карта заблокирована, свяжитесь с Вашим банком'
                    },
                    element: document.getElementById('cardNumber'), // Элемент в DOM-е
                    readonly: false                                           // Только чтение
                },
                expiryDate: {
                    label: 'Срок действия',
                    mask: "XX / XX",
                    rawValue: '',
                    active: false,
                    error: false,
                    errorMessage: '',
                    errorMessagesList: {
                        required: 'Введите срок действия карты',
                        expiration: 'Срок действия карты истек',
                        invalidExpiryDate: 'Неверная дата'
                    },
                    element: document.getElementById('expiryDate'),
                    readonly: true
                },
                cardCvv: {
                    label: 'CVV/CVC код',
                    mask: "XXX",
                    hideChar: "<span class='hide-char'></span>",
                    rawValue: '',
                    active: false,
                    elements: {"0":1,"1":2,"2":3,"3":4,"4":5,"5":6,"6":7,"7":8,"8":9,"9":0},    // Кнопки ввода
                    helper: false,                                                              // Показывать ли описание ( при активации )
                    helperDescription: 'CVV/CVC2 - последние <br> три цифры на обороте карты',  // Описание поля ( знак вопроса )
                    error: false,
                    errorMessage: '',
                    errorMessagesList: {
                        invalidCvv: 'Неверный код'
                    },
                    element: document.getElementById('cardCvv'),
                    hiddenElement: document.getElementById('cardCvvHidden'),
                    readonly: true
                }
            },
            keys: {
                "0": 48,
                "9": 57,
                "NUMPAD_0": 96,
                "NUMPAD_9": 105,
                "DELETE": 46,
                "BACKSPACE": 8,
                "ARROW_LEFT": 37,
                "ARROW_RIGHT": 39,
                "ARROW_UP": 38,
                "ARROW_DOWN": 40,
                "HOME": 36,
                "END": 35,
                "TAB": 9,
                "A": 65,
                "X": 88,
                "C": 67,
                "V": 86
            }
        }
    },
    filters: {},
    created() {
        this._setMatchMedia();
        window.addEventListener('resize', this._handlerWindowResize);
    },
    updated() {
        this.setTrigger();
    },
    mounted() {
        this._depthSetData();
        this._init();

        ////////////////////////////////////////////////////////////////////////
        //// FOR TESTING STATUSES ( remove this code )
        ////////////////////////////////////////////////////////////////////////

        // this.payForm.fields.cardNumber.rawValue = '1111 2222 3333 4444';
        // this.payForm.fields.expiryDate.rawValue = '12 / 44';

        // from bank confirmation
        // this.payForm.payment.status = true;
        // this.payForm.payment.statusProcess = false;
        // this.payForm.payment.bankFrame = '<div class="">From BANK</div>';

        // payment confirm success
        // this.payForm.payment.status = true;
        // this.payForm.payment.confirmation = true;
        // this.payForm.payment.confirmationProcess = false;
        // this.payForm.payment.message = 'Ваш счет пополнен на';
        // this.payForm.payment.description = '';

        // payment confirm not success
        // this.payForm.payment.status = true;
        // this.payForm.payment.confirmation = false;
        // this.payForm.payment.confirmationReady = true;
        // this.payForm.payment.confirmationProcess = false;
        // this.payForm.payment.message = 'Ошибка:';
        // this.payForm.payment.description = 'сумма пополнения больше допустимой, максимальная сумма пополнения 14 999 грн';

        ////////////////////////////////////////////////////////////////////////
    },
    methods: {
        // Send payment handler
        sendPayment() {
            if( !this.payForm.payment.statusProcess ) {
                console.log( 'send payment' );
                this.payForm.payment.statusProcess = true;
                // code here


                // for emulation text remove this code
                setTimeout( this.sendPaymentTestResult, 3000 );
            }
        },
        // for emulation text remove this function
        sendPaymentTestResult() {
            // success - show bank frame
            this.payForm.payment.status = true;
            this.payForm.payment.statusProcess = false;
            this.payForm.payment.bankFrame = '<div class="py-3 x-text-center" style="width:300px; height: 400px; background-color: antiquewhite;">From bank frame<a class="x-d-block mt-5" href="JavaScript:void(0);">Confirm payment</a></div>';

            setTimeout( this.sendPaymentConfirmationResult, 3000 );
        },
        // Send payment confirmation handler
        sendPaymentConfirmation() {
            if( this.payForm.payment && !this.payForm.confirmationProcess ) {
                console.log( 'send payment confirmation' );
                this.payForm.confirmationProcess = true;
                // code here
            }
        },
        sendPaymentConfirmationResult() {
            this.payForm.payment.confirmationProcess = true;

            // for emulation text remove this code
            setTimeout( this.completePaymentConfirmationResult, 3000 );
        },
        completePaymentConfirmationResult() {
            this.payForm.payment.confirmationReady = true;

            // for emulation text remove this code
            let randVal = Math.floor(Math.random() * (2 - 1 + 1) + 1);
            if( randVal == 1 ) {
                // success
                this.payForm.payment.status = true;
                this.payForm.payment.confirmation = true;
                this.payForm.payment.confirmationProcess = false;
                this.payForm.payment.message = 'Ваш счет пополнен на';
                this.payForm.payment.description = '';
            }
            else {
                this.payForm.payment.status = true;
                this.payForm.payment.confirmation = false;
                this.payForm.payment.confirmationProcess = false;
                this.payForm.payment.message = 'Ошибка:';
                this.payForm.payment.description = 'сумма пополнения больше допустимой, максимальная сумма пополнения 14 999 грн';
            }
        },
        _depthSetData() {
            if( XDATA.length !== 0 ) {
                this.payForm.XDATA = JSON.parse( XDATA );
                if( this.payForm.XDATA.hasOwnProperty('payForm') ) {
                    this.payForm = _.merge(this.payForm, this.payForm.XDATA.payForm);
                }
            }
        },
        _init() {
            // set elements
            this.payForm.fields.cardNumber.element = document.getElementById('cardNumber');
            this.payForm.fields.expiryDate.element = document.getElementById('expiryDate');
            this.payForm.fields.cardCvv.element = document.getElementById('cardCvv');
            this.payForm.fields.cardCvv.hiddenElement = document.getElementById('cardCvvHidden');

            // click outside cvv
            document.removeEventListener("click", this.clickOutsideCvv );
            document.addEventListener('click', this.clickOutsideCvv );
            // card number listeners
            this.payForm.fields.cardNumber.element.addEventListener('change', (event) => {this.cardNumberChange(event);});
            this.payForm.fields.cardNumber.element.addEventListener('keydown', (event) => {this.cardNumberKeyDown(event);});
            // expiry listeners
            this.payForm.fields.expiryDate.element.addEventListener('change', (event) => {this.expiryDateChange(event);});
            this.payForm.fields.expiryDate.element.addEventListener('keydown', (event) => {this.expiryDateKeyDown(event);});
            // cvv listeners
            this.payForm.fields.cardCvv.hiddenElement.addEventListener('change', (event) => {this.cardCvvHiddenChange(event);});
            this.payForm.fields.cardCvv.element.addEventListener('keydown', (event) => {this.cardCvvKeyDown(event);});



            // form ready
            this.payForm.config.dataReady = true;
        },
        clickOutsideCvv() {
            let specifiedElement = document.getElementById('cardCvv-body');
            if( specifiedElement != null ) {
                let isClickInside = specifiedElement.contains( event.target );
                if ( !isClickInside ) {
                    this.payForm.fields.cardCvv.active = false;
                    this.payForm.fields.cardCvv.helper = false;
                }
            }
        },
        checkField( names ) {
            let result = false;
            if( Array.isArray( names ) ) {
                for( let i = 0; i < names.length; i++ ) {
                    // if name isset in payForm fields
                    if( typeof this.payForm.fields[names[i]] !== 'undefined') {
                        result = true;
                        if( this.payForm.fields[names[i]].rawValue.length !== this.payForm.fields[names[i]].mask.length || this.payForm.fields[names[i]].error ) {
                            return false;
                        }
                    }
                }
            }
            return result;
        },
        getEmailLink(){
            return 'mailto: ' + this.payForm.config.emailLink;
        },
        payFormClass() {
            let classStr = '';
            if( this.payForm.config.dataReady ) {
                classStr += 'is-visible';
            }
            return classStr;
        },
        getCardTypesStr() {
            let resultString = '';
            for (const [key, value] of Object.entries( this.payForm.cardTypes )) {
                resultString += value.value;
                resultString += ', ';
            }
            return resultString.substring(0, resultString.length - 2);
        },
        inputBodyCardNumberClass() {
            let classString = '';
            if( this.payForm.fields.cardNumber.rawValue.length === 0 ) {
                classString += 'is-empty';
            }
            let cardNumberType = this.getCardType();
            if( cardNumberType && this.payForm.fields.cardNumber.rawValue.length >= 3 ) {
                classString += ' cardNumberType-' + cardNumberType.char;
            }
            // active
            if( this.payForm.fields.cardNumber.active ) {
                classString += ' is-active';
            }
            // error class
            if( this.payForm.fields.cardNumber.error ) {
                classString += ' has-error';
            }
            return classString;
        },
        getErrorDescription( commaLineBreak = true ) {
            return ( commaLineBreak ) ? this.payForm.payment.description.replace(',', ',<br/>') : this.payForm.payment.description;
        },
        isEmpty( fieldName ) {
            return ( this.payForm.fields[fieldName].rawValue.length === 0 ) ? true : false;
        },
        keyCodeFromEvent: function(e) {
            return e.which || e.keyCode;
        },
        keyIsCommandFromEvent: function(e) {
            return e.ctrlKey || e.metaKey;
        },
        keyIsNumber: function(e) {
            return this.keyIsTopNumber(e) || this.keyIsKeypadNumber(e);
        },
        keyIsTopNumber: function(e) {
            let keyCode = this.keyCodeFromEvent(e);
            return keyCode >= this.payForm.keys["0"] && keyCode <= this.payForm.keys["9"];
        },
        keyIsKeypadNumber: function(e) {
            let keyCode = this.keyCodeFromEvent(e);
            return keyCode >= this.payForm.keys["NUMPAD_0"] && keyCode <= this.payForm.keys["NUMPAD_9"];
        },
        keyIsDelete: function(e) {
            return this.keyCodeFromEvent(e) == this.payForm.keys["DELETE"];
        },
        keyIsBackspace: function(e) {
            return this.keyCodeFromEvent(e) == this.payForm.keys["BACKSPACE"];
        },
        keyIsDeletion: function(e) {
            return this.keyIsDelete(e) || this.keyIsBackspace(e);
        },
        keyIsArrow: function(e) {
            let keyCode = this.keyCodeFromEvent(e);
            return keyCode >= this.payForm.keys["ARROW_LEFT"] && keyCode <= this.payForm.keys["ARROW_DOWN"];
        },
        keyIsNavigation: function(e) {
            let keyCode = this.keyCodeFromEvent(e);
            return keyCode == this.payForm.keys["HOME"] || keyCode == this.payForm.keys["END"];
        },
        keyIsKeyboardCommand: function(e) {
            let keyCode = this.keyCodeFromEvent(e);
            return this.keyIsCommandFromEvent(e) &&
                (
                    keyCode == this.payForm.keys["A"] ||
                    keyCode == this.payForm.keys["X"] ||
                    keyCode == this.payForm.keys["C"] ||
                    keyCode == this.payForm.keys["V"]
                );
        },
        keyIsTab: function(e) {
            return this.keyCodeFromEvent(e) == this.payForm.keys["TAB"];
        },
        numbersOnlyString: function(string) {
            let numbersOnlyString = "";
            for(let i = 0; i < string.length; i++) {
                let currentChar = string.charAt(i),
                    isValid = !isNaN(parseInt(currentChar));
                if(isValid) { numbersOnlyString += currentChar; }
            }
            return numbersOnlyString;
        },
        applyFormatMask: function(string, mask) {
            let formattedString = "",
                numberPos = 0;
            for(let j = 0; j < mask.length; j++) {
                var currentMaskChar = mask[j];
                if(currentMaskChar == "X") {
                    var digit = string.charAt(numberPos);
                    if(!digit) {
                        break;
                    }
                    formattedString += string.charAt(numberPos);
                    numberPos++;
                } else {
                    formattedString += currentMaskChar;
                }
            }
            return formattedString;
        },
        cardTypeFromNumber: function(number) {
            let first = number.charAt(0);
            for (const [key, value] of Object.entries( this.payForm.cardTypes )) {
                if( first === value.char ) {
                    return value;
                }
            }
            return '';
        },
        caretStartPosition: function(element) {
            if(typeof element.selectionStart == "number") {
                return element.selectionStart;
            }
            return false;
        },
        caretEndPosition: function(element) {
            if(typeof element.selectionEnd == "number") {
                return element.selectionEnd;
            }
            return false;
        },
        setCaretPosition: function(element, caretPos) {
            if(element != null) {
                if(element.createRange) {
                    var range = element.createRange();
                    range.move('character', caretPos);
                    range.select();
                } else {
                    if(element.selectionStart) {
                        element.focus();
                        element.setSelectionRange(caretPos, caretPos);
                    } else {
                        element.focus();
                    }
                }
            }
        },
        normaliseCaretPosition: function(mask, caretPosition) {
            let numberPos = 0;
            if(caretPosition < 0 || caretPosition > mask.length) { return 0; }
            for(let i = 0; i < mask.length; i++) {
                if(i == caretPosition) { return numberPos; }
                if(mask[i] == "X") { numberPos++; }
            }
            return numberPos;
        },
        denormaliseCaretPosition: function(mask, caretPosition) {
            let numberPos = 0;
            if(caretPosition < 0 || caretPosition > mask.length) { return 0; }
            for(let i = 0; i < mask.length; i++) {
                if(numberPos == caretPosition) { return i; }
                if(mask[i] == "X") { numberPos++; }
            }
            return mask.length;
        },
        filterNumberOnlyKey: function(e) {
            let isNumber = this.keyIsNumber(e),
                isDeletion = this.keyIsDeletion(e),
                isArrow = this.keyIsArrow(e),
                isNavigation = this.keyIsNavigation(e),
                isKeyboardCommand = this.keyIsKeyboardCommand(e),
                isTab = this.keyIsTab(e);

            if(!isNumber && !isDeletion && !isArrow && !isNavigation && !isKeyboardCommand && !isTab) {
                e.preventDefault();
            }
        },
        digitFromKeyCode: function(keyCode) {
            if(keyCode >= this.payForm.keys["0"] && keyCode <= this.payForm.keys["9"]) {
                return keyCode - this.payForm.keys["0"];
            }
            if(keyCode >= this.payForm.keys["NUMPAD_0"] && keyCode <= this.payForm.keys["NUMPAD_9"]) {
                return keyCode - this.payForm.keys["NUMPAD_0"];
            }
            return null;
        },
        handleMaskedNumberInputKey: function(e, field, mask) {
            this.filterNumberOnlyKey(e);

            let keyCode = e.which || e.keyCode,
                element = e.target,
                caretStart = this.caretStartPosition(element),
                caretEnd = this.caretEndPosition(element),
                // Calculate normalised caret position
                normalisedStartCaretPosition = this.normaliseCaretPosition(mask, caretStart),
                normalisedEndCaretPosition = this.normaliseCaretPosition(mask, caretEnd),
                newCaretPosition = caretStart,
                isNumber = this.keyIsNumber(e),
                isDelete = this.keyIsDelete(e),
                isBackspace = this.keyIsBackspace(e);

            if ( isNumber || isDelete || isBackspace ) {
                e.preventDefault();
                let rawText = field.rawValue,
                    numbersOnly = this.numbersOnlyString(rawText),
                    digit = this.digitFromKeyCode(keyCode),
                    rangeHighlighted = normalisedEndCaretPosition > normalisedStartCaretPosition;
                // Remove values highlighted (if highlighted)
                if ( rangeHighlighted ) {
                    numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition) + numbersOnly.slice(normalisedEndCaretPosition));
                }
                // Forward Action
                if ( caretStart != mask.length ) {
                    // Insert number digit
                    if (isNumber && rawText.length <= mask.length) {
                        numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition) + digit + numbersOnly.slice(normalisedStartCaretPosition));
                        newCaretPosition = Math.max(
                            this.denormaliseCaretPosition(mask, normalisedStartCaretPosition + 1),
                            this.denormaliseCaretPosition(mask, normalisedStartCaretPosition + 2) - 1
                        );
                    }
                    // Delete
                    if (isDelete) {
                        numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition) + numbersOnly.slice(normalisedStartCaretPosition + 1));
                    }
                }
                // Backward Action
                if ( caretStart != 0 ) {
                    // Backspace
                    if( isBackspace && !rangeHighlighted ) {
                        numbersOnly = (numbersOnly.slice(0, normalisedStartCaretPosition - 1) + numbersOnly.slice(normalisedStartCaretPosition));
                        newCaretPosition = this.denormaliseCaretPosition(mask, normalisedStartCaretPosition - 1);
                    }
                }

                field.rawValue = this.applyFormatMask(numbersOnly, mask);
                this.setCaretPosition(element, newCaretPosition);
            }
        },
        cardCvvKeyDown: function( e ) {
            if( this.keyIsDeletion( e ) ) {
                this.payForm.fields.cardCvv.rawValue = this.payForm.fields.cardCvv.rawValue.slice(0, -1);
                this.payForm.fields.cardCvv.helper = false;
                if( this.payForm.fields.cardCvv.rawValue.length === 3 ) {
                    document.getElementById("btn-pay").focus();
                }
            }
            else {
                e.preventDefault();
                return false;
            }
        },
        cardCvvHiddenChange: function(e) {
            let value = e.target.value;
            value = this.numbersOnlyString( value );
            if( value.length === 3 ) {
                this.payForm.fields.cardCvv.rawValue = value;
                // update caret position on input
                this.setCaretPosition( this.payForm.fields.cardCvv.element, this.payForm.fields.cardCvv.rawValue.length );
                this.payForm.fields.cardCvv.active = false;
                window.setTimeout( this.fieldFocus, 1, this.payForm.fields.cardCvv.element);
            }
        },
        cvvClick: function() {
            this.validateCreditCard();
            this.validateExpiry();
            if( !this.payForm.fields.cardNumber.error && !this.payForm.fields.expiryDate.error ) {
                this.payForm.fields.cardCvv.active = true;
                this.payForm.fields.cardCvv.helper = false;
            }
            else if( !this.payForm.fields.cardNumber.error ) {
                this.payForm.fields.expiryDate.element.focus();
            }
            else {
                this.payForm.fields.cardNumber.element.focus();
            }
        },
        fieldFocus( element ) {
            element.focus();
        },
        cardCvvNumClick: function(e) {
            e.preventDefault();
            let value = e.target.value;
            if( value >= 0 && value <= 9 && this.payForm.fields.cardCvv.rawValue.length < 3 ) {
                this.payForm.fields.cardCvv.rawValue += value;
                let values = Object.values( this.payForm.fields.cardCvv.elements ),
                    buttons = {};
                values = values.sort(() => Math.random() - 0.5);
                for( let j = 0; j < values.length; j++ ) {
                    buttons[j]  = values[j];
                }
                this.payForm.fields.cardCvv.elements = buttons;
                // update caret position on input
                this.setCaretPosition( this.payForm.fields.cardCvv.element, this.payForm.fields.cardCvv.rawValue.length );
            }
            window.setTimeout( this.fieldFocus, 1, this.payForm.fields.cardCvv.element);
            this.payForm.fields.cardCvv.active = this.payForm.fields.cardCvv.rawValue.length === 3 ? false : true;
        },
        cardCvvNumClickDelete: function(e) {
            e.preventDefault();
            this.payForm.fields.cardCvv.rawValue = this.payForm.fields.cardCvv.rawValue.slice(0, -1);
            window.setTimeout( this.fieldFocus, 1, this.payForm.fields.cardCvv.element);
        },
        cardCvvMaskContent: function() {
            let value = '';
            for( let i = 0; i < this.payForm.fields.cardCvv.rawValue.length; i++ ) {
                value += this.payForm.fields.cardCvv.hideChar;
            }
            if( this.payForm.fields.cardCvv.rawValue.length === 0 ) {
                value += '<span class="trigger"></span>';
            }
            return value;
        },
        bindHiddenValue: function( length ) {
            let result = '';
            for( let i = 1; i <= length; i++ ) {
                if( i !== 3 ) {
                    result += ' ';
                }
            }
            return result;
        },
        handleCvvFocus: function(e) {

        },
        handleCvvBlur: function(e) {

        },
        setTrigger: function() {
            if( !this.payForm.payment.status && this.payForm.config.show ) {
                let container = document.querySelector('.x-input-masked-value'),
                    spans = container.querySelectorAll('span');
                for (let item of spans){
                    item.classList.remove('trigger');
                }
                let span = container.querySelector('span:last-child');
                if(span !==null) {
                    span.classList.add('trigger');
                }
            }
        },
        cvvInputBodyClass: function() {
            let stringClass = '';
            if( this.payForm.fields.cardCvv.rawValue.length === 0 ) {
                stringClass += ' is-empty';
            }
            if( this.payForm.fields.cardCvv.active ) {
                stringClass += ' is-active';
            }
            stringClass += ' value-length-' + this.payForm.fields.cardCvv.rawValue.length;
            return stringClass;
        },
        /*
        * Expiry functions
        * */
        validateExpiry() {
            if( !this.payForm.fields.expiryDate.readonly ) {
                let month = '',
                    year = '',
                    expiry = '';
                if( this.payForm.fields.expiryDate.rawValue.indexOf('/', 0) !== -1 ) {
                    expiry = this.payForm.fields.expiryDate.rawValue.split("/");
                    month = expiry[0];
                    year = expiry[1];
                }
                else {
                    month = this.payForm.fields.expiryDate.rawValue;
                }
                // required
                if( this.payForm.fields.expiryDate.rawValue.replace(/\s+/g, '').length < 5 ) {
                    this.payForm.fields.expiryDate.error = true;
                    this.payForm.fields.expiryDate.errorMessage = this.payForm.fields.expiryDate.errorMessagesList.required;
                }
                // expiration
                else if( !this.isExpiryValid( month, year ) ) {
                    this.payForm.fields.expiryDate.error = true;
                    this.payForm.fields.expiryDate.errorMessage = this.payForm.fields.expiryDate.errorMessagesList.expiration;
                }
                else {
                    this.payForm.fields.expiryDate.error = false;
                }
            }
        },
        handleExpiryFocus(e) {
            this.payForm.fields.expiryDate.active = true;
            this.validateCreditCard();
            if( this.payForm.fields.cardNumber.error ) {
                this.payForm.fields.cardNumber.element.focus();
            }
        },
        handleExpiryBlur(e) {
            this.payForm.fields.expiryDate.active = false;
            this.validateExpiry();
        },
        handleExpiryKey: function(e) {
            this.handleMaskedNumberInputKey(e, this.payForm.fields.expiryDate, this.payForm.fields.expiryDate.mask);
        },
        expiryDateKeyDown: function(e) {
            if( !this.payForm.fields.expiryDate.readonly ) {
                this.handleExpiryKey(e);
                let val = this.payForm.fields.expiryDate.rawValue;
                if( val.length == 1 && parseInt(val) > 1 && this.keyIsNumber(e) ) {
                    this.payForm.fields.expiryDate.rawValue = this.applyFormatMask("0" + val, this.payForm.fields.expiryDate.mask );
                }
                if( val.length > 1 && val.charAt(0) == 1 && val.charAt(1) > 2 ) {
                    this.payForm.fields.expiryDate.rawValue = this.applyFormatMask(val.substr(0, 1), this.payForm.fields.expiryDate.mask );
                }
                if( this.payForm.fields.expiryDate.rawValue.replace(/\s+/g, '').length === 5 ) {
                    this.validateExpiry();
                    if( !this.payForm.fields.expiryDate.error ) {
                        this.payForm.fields.cardCvv.active = true;
                        window.setTimeout( this.fieldFocus, 1, this.payForm.fields.cardCvv.element);
                    }
                }
            }
        },
        expiryDateChange: function(e) {
            this.handleExpiryPaste(e);
        },
        refreshExpiryMonthYearInput: function() {
            if( this.payForm.fields.expiryDate.rawValue.length > 1 ) {
                if( this.payForm.fields.expiryDate.rawValue.length >= 5 ) {
                    let date = this.payForm.fields.expiryDate.rawValue.substring(0, 2) + '/' + this.payForm.fields.expiryDate.rawValue.substring(this.payForm.fields.expiryDate.rawValue.length - 2, this.payForm.fields.expiryDate.rawValue.length);
                    this.payForm.fields.expiryDate.rawValue = date;
                }
                if( this.payForm.fields.expiryDate.rawValue.charAt(0) == 1 && this.payForm.fields.expiryDate.rawValue.charAt(1) > 2 ) {
                    this.payForm.fields.expiryDate.rawValue = this.applyFormatMask(this.payForm.fields.expiryDate.rawValue.substr(0, 1), this.payForm.fields.expiryDate.mask );
                }
            }
            let numbersOnly = this.numbersOnlyString( this.payForm.fields.expiryDate.rawValue ),
                formattedNumber = this.applyFormatMask( numbersOnly, this.payForm.fields.expiryDate.mask );
            this.payForm.fields.expiryDate.rawValue = formattedNumber;
        },
        handleExpiryPaste() {
            this.refreshExpiryMonthYearInput();
            if( this.payForm.fields.expiryDate.rawValue.replace(/\s+/g, '').length === 5 ) {
                this.validateExpiry();
                if( !this.payForm.fields.expiryDate.error ) {
                    this.payForm.fields.cardCvv.active = true;
                    window.setTimeout( this.fieldFocus, 1, this.payForm.fields.cardCvv.element);
                }
            }
        },
        isValidMonth( expiryMonth ) {
            return ( expiryMonth >= 1 && expiryMonth <= 12);
        },
        isExpiryValid( month, year ) {
            let today = new Date(),
                currentMonth = (today.getMonth() + 1),
                currentYear = "" + today.getFullYear(),
                enteredYear = "";
            if ( ( "" + year).trim().length == 2 ) {
                let yearFirstTwo = currentYear.substring(0, 2);
                    enteredYear += yearFirstTwo.toString() + year.trim();
            }
            else {
                enteredYear += "" + year.trim();
            }
            currentMonth = parseInt(currentMonth);
            currentYear = parseInt(currentYear);
            month = parseInt(month);
            enteredYear = parseInt(enteredYear);
            return this.isValidMonth(month) && ((enteredYear > currentYear) || (enteredYear == currentYear && month >= currentMonth));
        },
        /*
        * Card number functions
        * */
        getCardNumber: function() {
            return this.payForm.fields.cardNumber.rawValue;
        },
        setCardNumber: function( value ) {
            this.payForm.fields.cardNumber.rawValue = value;
        },
        getCardType: function() {
            return this.cardTypeFromNumber( this.getCardNumber() );
        },
        cardNumberKeyDown:function(e) {
            this.handleCreditCardNumberKey(e, this.payForm.fields.cardNumber, this.payForm.fields.cardNumber.mask);
            if( this.payForm.fields.cardNumber.rawValue.replace(/\s+/g, '').length === 16 ) {
                this.validateCreditCard();
                if( !this.payForm.fields.cardNumber.error ) {
                    window.setTimeout( this.fieldFocus, 1, this.payForm.fields.expiryDate.element);
                }
            }
        },
        cardNumberChange:function(e) {
            this.handleCreditCardNumberPaste(e);
            window.setTimeout( this.setInputBodyCardNumberClass, 1);

        },
        setInputBodyCardNumberClass:function() {
            document.getElementById("x-input-body-cardNumber").className = 'x-input-body x-input-body-cardNumber x-input-body-wide ' + this.inputBodyCardNumberClass();
        },
        refreshCreditCardNumberFormat: function(e) {
            let numbersOnly = this.numbersOnlyString( this.getCardNumber() ),
                formattedNumber = this.applyFormatMask(numbersOnly, this.payForm.fields.cardNumber.mask);
            this.setCardNumber( formattedNumber );
        },
        handleCreditCardNumberKey: function(e, field, cardMask) {
            this.handleMaskedNumberInputKey(e, field, cardMask);
        },
        handleCreditCardNumberFocus(e) {
            this.payForm.fields.cardNumber.active = true;
        },
        handleCreditCardNumberBlur(e) {
            this.validateCreditCard();
        },
        handleCreditCardNumberPaste(e) {
            this.refreshCreditCardNumberFormat();
            if( this.payForm.fields.cardNumber.rawValue.replace(/\s+/g, '').length === 16 ) {
                this.validateCreditCard();
                if( !this.payForm.fields.cardNumber.error ) {
                    window.setTimeout( this.fieldFocus, 1, this.payForm.fields.expiryDate.element);
                }
            }
        },
        validateCreditCard() {
            this.payForm.fields.cardNumber.active = false;
            // required
            if( this.payForm.fields.cardNumber.rawValue.replace(/\s+/g, '').length < 16 ) {
                this.payForm.fields.cardNumber.error = true;
                this.payForm.fields.cardNumber.errorMessage = this.payForm.fields.cardNumber.errorMessagesList.required;
            }
            // invalid card value
            else if( !this.isValidCreditCardValue( this.payForm.fields.cardNumber.rawValue ) ) {
                this.payForm.fields.cardNumber.error = true;
                this.payForm.fields.cardNumber.errorMessage = this.payForm.fields.cardNumber.errorMessagesList.invalidCard;
            }
            else {
                this.payForm.fields.cardNumber.error = false;
            }
            // enable for edit expiry
            this.payForm.fields.expiryDate.readonly = ( this.payForm.fields.cardNumber.error ) ? true : false;
        },
        isValidCreditCardValue( value ) {
            if (/[^0-9-\s]+/.test(value)) return false;

            // The Luhn Algorithm. It so pretty.
            let nCheck = 0,
                nDigit = 0,
                bEven = false;
            value = value.replace(/\D/g, "");

            for ( let n = value.length - 1; n >= 0; n-- ) {
                let cDigit = value.charAt(n);
                nDigit = parseInt(cDigit, 10);
                if ( bEven ) {
                    if ( ( nDigit *= 2 ) > 9 ) nDigit -= 9;
                }
                nCheck += nDigit;
                bEven = !bEven;
            }
            return ( nCheck % 10 ) == 0;
        },
        resetForm() {
            this.payForm.payment.status = false;
            this.payForm.payment.statusProcess = false;
            this.payForm.payment.confirmation = false;
            this.payForm.payment.confirmationProcess = false;
            this.payForm.payment.confirmationReady = false;
            this.payForm.payment.message = '';
            this.payForm.payment.description = '';
            this.payForm.payment.bankFrame = '';

            this.payForm.fields.cardNumber.rawValue = '';
            this.payForm.fields.cardNumber.active = false;
            this.payForm.fields.cardNumber.error = false;
            this.payForm.fields.cardNumber.errorMessage = '';

            this.payForm.fields.expiryDate.rawValue = '';
            this.payForm.fields.expiryDate.active = false;
            this.payForm.fields.expiryDate.error = false;
            this.payForm.fields.expiryDate.errorMessage = '';
            this.payForm.fields.expiryDate.readonly = true;

            this.payForm.fields.cardCvv.rawValue = '';
            this.payForm.fields.cardCvv.active = false;
            this.payForm.fields.cardCvv.error = false;
            this.payForm.fields.cardCvv.errorMessage = '';
            this.payForm.fields.cardCvv.readonly = true;
            this.payForm.fields.cardCvv.elements = {"0":1,"1":2,"2":3,"3":4,"4":5,"5":6,"6":7,"7":8,"8":9,"9":0};

            setTimeout( this._init, 250 );
        },
        _handlerWindowResize() {
            this._setMatchMedia();
        },
        _setMatchMedia() {
            this.payForm.config.matchMedia.sm = window.matchMedia("(min-width: 576px)").matches;
            this.payForm.config.matchMedia.md = window.matchMedia("(min-width: 768px)").matches;
            this.payForm.config.matchMedia.lg = window.matchMedia("(min-width: 992px)").matches;
            this.payForm.config.matchMedia.xl = window.matchMedia("(min-width: 1200px)").matches;
        },

    }
});